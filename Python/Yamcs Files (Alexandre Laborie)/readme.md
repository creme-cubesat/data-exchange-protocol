

# Scripts to be used to send and received TM and TC other a UDP or TCP link. 


# Setup 

>pip install crc  
pip install reed-solomon-ccsds   
pip install datetime


# Main folder
### UDP_send_CCSDS_packet.py

>Send continuoulsy a PUS packet (3,25) over a UDP link.  
Manage IP adress and port in the script. 
The CUC time in the packet should be updated. 


### TCP_send_CCSDS_packet.py

>Send continuoulsy a PUS packet (3,25) over a TCP link.  
Manage IP adress and port in the script. 
The CUC time in the packet should be updated. 

### UDP_send_CCSDS_frame.py

>Send continuoulsy a PUS CADU containing a packet (3,25) over a UDP link.  
Manage IP adress and port in the script. 
The CUC time in the packet should be updated. 

### TCP_send_CCSDS_frame.py

>Send continuoulsy a PUS CADU containing a packet (3,25) over a TCP link.  
Manage IP adress and port in the script. 
The CUC time in the packet should be updated. 

### UDP_receive.py

>Display telecommand if received on a UDP link. 

### TCP_receive.py

>Display telecommand if received on a TCP link. 

### UDP_basic_packet_telecommunication.py

>Ongoing...
