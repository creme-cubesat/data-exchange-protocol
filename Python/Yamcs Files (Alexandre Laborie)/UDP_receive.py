# coding: utf-8
import socket

#---
# Code developed for tests purposes. 
# Display received telecommand vie UDP link. 
#---

host = 'localhost'  # own IP adress
port = 10027           # own port

UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
UDPServerSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #to avoid error: [Errno 98] Address already in use
UDPServerSocket.bind((host, port))

print("UDP server up and listening on port " + str(port))

while True : 
    try:
        TC = UDPServerSocket.recv(2000)
        print("Received Telecommand : ")
        print(TC.hex())
    
    except KeyboardInterrupt: 
        print("Fermeture de la connexion")
        UDPServerSocket.close()
        break
