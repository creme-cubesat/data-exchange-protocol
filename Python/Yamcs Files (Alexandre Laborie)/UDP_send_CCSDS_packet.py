# coding: utf-8
import socket
import time
from crc import CrcCalculator, Configuration
from datetime import datetime, timedelta

#---
# Code developed for tests purposes. 
# Send a TM_3_25 pus packet with incremental pus time using a UDP link. 
#---

host = 'localhost'  # Distant IP adress (receiver)
port = 10028   #10015           # Distant port (receiver)

# Create a UDP socket at client side
UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
UDPClientSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #to avoid error: [Errno 98] Address already in use
UDPClientSocket.connect((host, port))

print("Connexion établie avec le serveur sur le port {}".format(port))


# CRC calculation parameters
width = 16
poly=0x1021
init_value=0xffff
final_xor_value=0x00
reverse_input=False
reverse_output=False
configuration = Configuration(width, poly, init_value, final_xor_value, reverse_input, reverse_output)
use_table = True
crc_calculator = CrcCalculator(configuration, use_table)

while True : 
    try:
        time.sleep(2)

        print("*******")

        space_packet = bytearray.fromhex("080ac40b002d1003190002")

        # Add CUC time
        now = datetime.utcnow()
        TAI_ref = datetime(1958,1,1)
        duration = now - TAI_ref
        elapsed_seconds = int(duration.total_seconds())-1800 # artificially decreasing the packet time for tests reasons
        CUC_time = elapsed_seconds.to_bytes(4, 'big')
        space_packet += CUC_time

        #Add end of packet example
        space_packet += bytearray.fromhex("0000000000006c0000000000000000000000000000000000000000000080100000e5a8")
        
        # Add CCSDS CRC
        checksum = crc_calculator.calculate_checksum(space_packet)
        checksum_bytes = checksum.to_bytes(2, 'big')
        print('Checksum value : ' + str(checksum_bytes.hex()))
        space_packet += checksum_bytes
        print('Space packet with CRC : ' + str(space_packet.hex()))
        
        # Send packet via the UDP link
        UDPClientSocket.sendto(space_packet, (host, port))

    except KeyboardInterrupt: 
        print("Fermeture de la connexion")
        UDPClientSocket.close()
        break



