# coding: utf-8
import socket

#---
# Code developed for tests purposes. 
# Display received telecommand via TCP link. 
#---

host = '127.0.0.1'  # own IP adress
port = 10018             # own port

TCPServer = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
TCPServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #to avoid error: [Errno 98] Address already in use
TCPServer.bind((host, port))
TCPServer.listen(1)


print("TCP server up and listening on port " + str(port))

while True : 

    try:
        client, adress = TCPServer.accept()
        print("Client connected")
        TC = client.recv(2000)
        print("Received Telecommand : ")
        print(TC.hex())
    
    except KeyboardInterrupt: 
        print("Connection closed")
        TCPServer.close()
        break
