# coding: utf-8
import socket
from crc import CrcCalculator, Configuration

host_TM = '169.254.125.130'
port_TM = 10015

UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
UDPClientSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #to avoid error: [Errno 98] Address already in use
UDPClientSocket.connect((host_TM, port_TM))

print("Connexion établie avec le serveur sur le port {}".format(port_TM))

host_TC = '169.254.125.129'
port_TC = 10025

UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
UDPServerSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #to avoid error: [Errno 98] Address already in use
UDPServerSocket.bind((host_TC, port_TC))

print("UDP server up and listening on port {}".format(port_TC))



width = 16
poly=0x1021
init_value=0xffff
final_xor_value=0x00
reverse_input=False
reverse_output=False

configuration = Configuration(width, poly, init_value, final_xor_value, reverse_input, reverse_output)

use_table = True
crc_calculator = CrcCalculator(configuration, use_table)

while True : 
    print("**********")

    TC = UDPServerSocket.recvfrom(1000)[0]
    
    # TC CRC check
    
    if TC.hex()[14:18] == 'f001':
        print("TC_240_1 received :" + TC.hex())
        TM_1_1 = bytearray.fromhex("080ac4040011100101000200000001000000180ac0132807")
        UDPClientSocket.sendto(TM_1_1, (host_TM, port_TM))
                
        TM_240_2 = bytearray.fromhex("080ac403001010f00200020000000000000")
        GENE_AR_BATTTEMP = bytearray.fromhex(TC.hex()[20:28])
        
        TM_240_2 += GENE_AR_BATTTEMP

        checksum = crc_calculator.calculate_checksum(TM_240_2)
        checksum_bytes = checksum.to_bytes(2, 'big')

        TM_240_2_with_CRC16 = TM_240_2
        TM_240_2_with_CRC16 += checksum_bytes
        
        print('TM_240_2 : ' + str(TM_240_2.hex()))
        UDPClientSocket.sendto(TM_240_2, (host_TM, port_TM))


print("Fermeture de la connexion")
connectionWithServer.close()
