
# coding: utf-8
import socket
import time
from crc import CrcCalculator, Configuration
from datetime import datetime, timedelta

host = '127.0.0.1'
port = 10017



# Create a UDP socket at client side

TCPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
TCPServerSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #to avoid error: [Errno 98] Address already in use
TCPServerSocket.bind((host, port))

TCPServerSocket.listen(1)

print("Waiting for a client to connect...")
connection, adress = TCPServerSocket.accept()
print("Connexion etablie avec : ")
print(adress)


width = 16
poly=0x1021
init_value=0xffff
final_xor_value=0x00
reverse_input=False
reverse_output=False
configuration = Configuration(width, poly, init_value, final_xor_value, reverse_input, reverse_output)
use_table = True
crc_calculator = CrcCalculator(configuration, use_table)

while True : 
    try:
        time.sleep(2)

        print("*******")

        space_packet = bytearray.fromhex("080ac40b002d1003190002")

        # Add CUC time
        now = datetime.utcnow()
        TAI_ref = datetime(1958,1,1)
        duration = now - TAI_ref
        elapsed_seconds = int(duration.total_seconds())-1800 # artificially decreasing the packet time for tests reasons
        CUC_time = elapsed_seconds.to_bytes(4, 'big')
        space_packet += CUC_time

        #Add end of packet example
        space_packet += bytearray.fromhex("0000000000006c0000000000000000000000000000000000000000000080100000e5a8")
        
        # Add CCSDS CRC
        checksum = crc_calculator.calculate_checksum(space_packet)
        checksum_bytes = checksum.to_bytes(2, 'big')
        print('Checksum value : ' + str(checksum_bytes.hex()))
        space_packet += checksum_bytes
        print('Space packet with CRC : ' + str(space_packet.hex()))
        
        connection.sendall(space_packet)

    except KeyboardInterrupt:
        print("Fermeture de la connexion")
        TCPServerSocket.close()
        break

