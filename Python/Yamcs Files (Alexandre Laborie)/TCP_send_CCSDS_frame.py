
# coding: utf-8
import errno
import socket
import time
from crc import CrcCalculator, Configuration
from datetime import datetime, timedelta
from reed_solomon_ccsds import encode


host = '127.0.0.1'
port = 10018


# Create a UDP socket at client side

TCPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
TCPServerSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #to avoid error: [Errno 98] Address already in use
TCPServerSocket.bind((host, port))

TCPServerSocket.listen(1)


print("Waiting for a client to connect...")
connection, adress = TCPServerSocket.accept()
print("Connection established with : ")
print(adress)

width = 16
poly=0x1021
init_value=0xffff
final_xor_value=0x00
reverse_input=False
reverse_output=False

configuration = Configuration(width, poly, init_value, final_xor_value, reverse_input, reverse_output)

use_table = True
crc_calculator = CrcCalculator(configuration, use_table)

while True : 
    try:

        time.sleep(2)

        # ***
        # space_packet building       --packet 1
        # ***

        space_packet = bytearray.fromhex("080ac40b002d1003190002")

        # Add CUC time
        now = datetime.utcnow()
        TAI_ref = datetime(1958,1,1)
        duration = now - TAI_ref
        elapsed_seconds = int(duration.total_seconds())-3600
        CUC_time = elapsed_seconds.to_bytes(4, 'big')
        space_packet += CUC_time

        #Add end of packet example
        space_packet += bytearray.fromhex("0000000000006c0000000000000000000000000000000000000000000080100000e5a8")
        
        # Add CCSDS CRC
        checksum = crc_calculator.calculate_checksum(space_packet)
        checksum_bytes = checksum.to_bytes(2, 'big')
        print('Checksum value : ' + str(checksum_bytes.hex()))
        space_packet += checksum_bytes
        print('Space packet with CRC : ' + str(space_packet.hex()))
        ####################################

        # ***
        # space_packet building       --packet 2
        # ***
# ***
        # space_packet building       --packet 1
        # ***

        space_packet2 = bytearray.fromhex("080ac40b002d1003190002")

        # Add CUC time
        now = datetime.utcnow()
        TAI_ref = datetime(1958,1,1)
        duration = now - TAI_ref
        elapsed_seconds = int(duration.total_seconds())-1800
        CUC_time = elapsed_seconds.to_bytes(4, 'big')
        space_packet2 += CUC_time

        #Add end of packet example
        space_packet2 += bytearray.fromhex("0000000000006c0000000000000000000000000000000000000000000080100000e5a8")
        
        # Add CCSDS CRC
        checksum = crc_calculator.calculate_checksum(space_packet2)
        checksum_bytes = checksum.to_bytes(2, 'big')
        print('Checksum value : ' + str(checksum_bytes.hex()))
        space_packet2 += checksum_bytes
        print('Space packet with CRC : ' + str(space_packet2.hex()))
        ####################################

        # ***
        # frame building
        # ***

        TM_frame_3_25 = bytearray.fromhex("3fc1e30b1800")
        idle_packet = bytearray.fromhex("07ffffff03ecaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa0100c000")
        print("idle packet size: "+str(len(idle_packet)))
        TM_frame_3_25 += space_packet
        TM_frame_3_25 += space_packet2
        TM_frame_3_25 += idle_packet
        print('Frame len: '+str(len(TM_frame_3_25)))


        #RS(223,255) encoding
        #TM_frame_3_25 = encode(TM_frame_3_25, True, 5)
        print("Sending frame...")
        print(TM_frame_3_25.hex())
        
        connection.sendall(TM_frame_3_25)
        

    except KeyboardInterrupt:
        print("Fermeture de la connexion")
        TCPServerSocket.close()
        break
        
    except IOError as e:
        print("Waiting for a client to connect...")
        connection, adress = TCPServerSocket.accept()
        print("Connexion etablie avec : ")
        print(adress)
