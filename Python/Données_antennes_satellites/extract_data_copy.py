
import matplotlib.pyplot as plt
import numpy as np 
from mpl_toolkits.mplot3d import Axes3D


def read_antenna_data_file(filepath): 
    f=open(filepath,"r")
    lines=f.readlines()
    range = []
    for x in lines:
        split_line = x.split('       ')
        if len(split_line)>= 6:
            range.append(float(split_line[2]))
    f.close()
    return np.array(range)

range_list = read_antenna_data_file('range.txt')
print('Mean range [km] = ' + str(np.mean(range_list)))
print('Percentile 50 range [km] = ' + str(np.percentile(range_list, 50)))
print('Percentile 75 range [km] = ' + str(np.percentile(range_list, 75)))
