#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 28 11:01:42 2022

@author: d.opoka
"""

# Calculating the Volume of the Scatter points of the radiation pattern of antenna.

import matplotlib.pyplot as plt
import numpy as np 
from mpl_toolkits.mplot3d import Axes3D
import scipy.spatial as ss

# %%
'-------------------------------- FUNCTIONS ----------------------------------'
def compute_angle_vector(v1, v2): 
    dot_product = v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2]
    norm_v1 = np.sqrt(v1[0]**2 + v1[1]**2 + v1[2]**2)
    norm_v2 = np.sqrt(v2[0]**2 + v2[1]**2 + v2[2]**2)
    angle =  np.arccos(dot_product/(norm_v1*norm_v2))
    return angle

def compute_depointing_angle(theta, phi): 
    depointing_angle_mat = []
    for i in range(len(theta)):
        t = theta[i]*np.pi/180
        p = phi[i] *np.pi/180
        pointing_vector = [np.sin(t)*np.cos(p), np.sin(t)*np.sin(p), np.cos(t)]
        if t >= 0: 
            antenna_normal_direction = [0,1,0]
        else:
            antenna_normal_direction = [0,-1,0]

        depointing_angle = compute_angle_vector(pointing_vector, antenna_normal_direction)*180/np.pi
        depointing_angle_mat.append(depointing_angle)

    mean_depointing_loss = []
    percentile75_depointing_loss = []
    percentile95_depointing_loss = []
    step_lissage = 3
    angle_step = np.arange(0,90,step_lissage)
    for k in angle_step: 
        gain_per_degree = []
        for i in range(len(depointing_angle_mat)):
            if k < depointing_angle_mat[i] and k+step_lissage > depointing_angle_mat[i]:
                gain_per_degree.append(PCG_gain[i])
        gain_per_degree = np.array(gain_per_degree)
        mean_depointing_loss.append(np.mean(gain_per_degree))
        percentile75_depointing_loss.append(np.percentile(gain_per_degree, 10))
        percentile95_depointing_loss.append(np.percentile(gain_per_degree, 5))

    return [depointing_angle_mat, mean_depointing_loss, percentile75_depointing_loss, percentile95_depointing_loss, angle_step]

def exp_func(a, b, x):
    return a * np.exp(+b * x)

def read_antenna_data_file(filepath): 
    f=open(filepath,"r")
    lines=f.readlines()
    theta = []
    phi = []
    PCG_gain=[]
    for x in lines:
        split_line = x.split('\t')
        theta.append(float(split_line[0]))
        phi.append(float(split_line[1]))
        PCG_gain.append(float(split_line[4]))
    f.close()
    return [np.array(theta), np.array(phi), np.array(PCG_gain)]

def plot_radiation_pattern(theta, phi, PCG_gain):
    PCG_gain_scal = np.power(10, PCG_gain/10)
    x=[]
    y=[]
    z=[]
    for i in range(len(theta)):
        t = np.deg2rad(theta[i])
        p = np.deg2rad(phi[i])
        x.append(PCG_gain_scal[i]*np.sin(t)*np.cos(p))
        y.append(PCG_gain_scal[i]*np.sin(t)*np.sin(p))
        z.append(PCG_gain_scal[i]*np.cos(t))

    fig= plt.figure()
    ax = Axes3D(fig)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.view_init(90, 0)
    scat = ax.scatter3D(np.array(x) / np.max(x), np.array(y) / np.max(y), np.array(z) / np.max(z), c = PCG_gain_scal)
    fig.colorbar(scat)
    ax.set_title("Radiation pattern satellite combined antennas")
    
    return np.array(x), np.array(y), np.array(z)

def plot_depointing_gain(theta, phi, PCG_gain): 
    [depointing_angle_mat, mean_depointing_loss, percentile75_depointing_loss,percentile95_depointing_loss,angle_step] = compute_depointing_angle(theta, phi)
    fig= plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(depointing_angle_mat, PCG_gain, alpha = 0.1)
    ax.plot(angle_step+(angle_step[1]-angle_step[0])/2, mean_depointing_loss, 'r', label='Mean per angle degree [dB]', linewidth = 4)
    ax.plot(angle_step+(angle_step[1]-angle_step[0])/2, percentile75_depointing_loss, 'g', label='Percentile 90 per angle degree [dB]', linewidth = 4)
    ax.plot(angle_step+(angle_step[1]-angle_step[0])/2, percentile95_depointing_loss, 'k', label='Percentile 95 per angle degree [dB]', linewidth = 4)
    ax.legend()
    ax.set_xlabel('Depointing angle[deg]')
    ax.set_ylabel('Antennas gain[dBi]')
    ax.set_title("Radiation pattern satellite combined antennas")
    
    return angle_step, mean_depointing_loss



# %%

plt.close('all')

filepath = 'BandeS_PCD-PCG-2218M-norme.txt'

[theta, phi, PCG_gain] = read_antenna_data_file(filepath)
X_rad, Y_rad, Z_rad = plot_radiation_pattern(theta, phi, PCG_gain)

# Inputting normalized points 
points = np.zeros((len(X_rad), 3))
for ind in range(len(X_rad)):
    points[ind][0] = X_rad[ind] / np.max(np.abs(X_rad))
    points[ind][1] = Y_rad[ind] / np.max(np.abs(Y_rad))
    points[ind][2] = Z_rad[ind] / np.max(np.abs(Z_rad))

hull = ss.ConvexHull(points)
hull_volume = hull.volume
print('volume inside points is: ', hull_volume)

angle_step, mean_depointing_loss = plot_depointing_gain(theta, phi, PCG_gain)

# %%

fig= plt.figure()

error = 100
n_poly = 0
while error >= 5:
    n_poly += 1
    
    xdata = angle_step+(angle_step[1]-angle_step[0])/2
    ydata = mean_depointing_loss
    polyfit_data = np.polyfit(xdata, ydata, n_poly)
    polyfit_data = np.poly1d(polyfit_data)
    
    error = np.abs((polyfit_data(xdata) - ydata) / ydata) * 100
    error = np.mean(error)
    print(error)

x_linspace = np.linspace(0, 90, 1000)

plt.plot(x_linspace, polyfit_data(x_linspace), 'r')

# plt.plot(angle_step+(angle_step[1]-angle_step[0])/2, mean_depointing_loss, 'b', label='Mean per angle degree [dB]', linewidth = 4)
plt.plot(angle_step, mean_depointing_loss, 'b', label='Mean per angle degree [dB]', linewidth = 4)

plt.show()