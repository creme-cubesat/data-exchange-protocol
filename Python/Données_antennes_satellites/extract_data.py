
import matplotlib.pyplot as plt
import numpy as np 
from mpl_toolkits.mplot3d import Axes3D

def compute_angle_vector(v1, v2): 
    dot_product = v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2]
    norm_v1 = np.sqrt(v1[0]**2 + v1[1]**2 + v1[2]**2)
    norm_v2 = np.sqrt(v2[0]**2 + v2[1]**2 + v2[2]**2)
    angle =  np.arccos(dot_product/(norm_v1*norm_v2))
    return angle

def compute_depointing_angle(theta, phi): 
    depointing_angle_mat = []
    for i in range(len(theta)):
        t = theta[i]*np.pi/180
        p = phi[i] *np.pi/180
        pointing_vector = [np.sin(t)*np.cos(p), np.sin(t)*np.sin(p), np.cos(t)]
        if t >= 0: 
            antenna_normal_direction = [0,1,0]
        else:
            antenna_normal_direction = [0,-1,0]

        depointing_angle = compute_angle_vector(pointing_vector, antenna_normal_direction)*180/np.pi
        depointing_angle_mat.append(depointing_angle)

    mean_depointing_loss = []
    percentile75_depointing_loss = []
    percentile95_depointing_loss = []
    step_lissage = 3
    angle_step = np.arange(0,90,step_lissage)
    for k in angle_step: 
        gain_per_degree = []
        for i in range(len(depointing_angle_mat)):
            if k < depointing_angle_mat[i] and k+step_lissage > depointing_angle_mat[i]:
                gain_per_degree.append(PCG_gain[i])
        gain_per_degree = np.array(gain_per_degree)
        mean_depointing_loss.append(np.mean(gain_per_degree))
        percentile75_depointing_loss.append(np.percentile(gain_per_degree, 10))
        percentile95_depointing_loss.append(np.percentile(gain_per_degree, 5))

    return [depointing_angle_mat, mean_depointing_loss, percentile75_depointing_loss, percentile95_depointing_loss, angle_step]

def spherical2cartesian(theta, phi) : 
    Xsat = []
    Ysat = []
    Zsat = []
    for i in range(len(theta)):
        t = theta[i]*np.pi/180
        p = phi[i] *np.pi/180 
        Xsat.append(np.sin(t)*np.cos(p))
        Xsat.append(np.sin(t)*np.sin(p))
        Xsat.append(np.cos(t))
    return [Xsat, Ysat, Zsat]

def read_antenna_data_file(filepath): 
    f=open(filepath,"r")
    lines=f.readlines()
    theta = []
    phi = []
    PCG_gain=[]
    for x in lines:
        split_line = x.split('\t')
        theta.append(float(split_line[0]))
        phi.append(float(split_line[1]))
        PCG_gain.append(float(split_line[4]))
    f.close()
    return [np.array(theta), np.array(phi), np.array(PCG_gain)]

def plot_depointing_gain(theta, phi, PCG_gain): 
    [depointing_angle_mat, mean_depointing_loss, percentile75_depointing_loss,percentile95_depointing_loss,angle_step] = compute_depointing_angle(theta, phi)
    fig= plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(depointing_angle_mat, PCG_gain, alpha = 0.1)
    ax.plot(angle_step+(angle_step[1]-angle_step[0])/2, mean_depointing_loss, 'r', label='Mean per angle degree [dB]', linewidth = 4)
    ax.plot(angle_step+(angle_step[1]-angle_step[0])/2, percentile75_depointing_loss, 'g', label='Percentile 90 per angle degree [dB]', linewidth = 4)
    ax.plot(angle_step+(angle_step[1]-angle_step[0])/2, percentile95_depointing_loss, 'k', label='Percentile 95 per angle degree [dB]', linewidth = 4)
    ax.legend()
    ax.set_xlabel('Depointing angle[deg]')
    ax.set_ylabel('Antennas gain[dBi]')
    ax.set_title("Radiation pattern satellite combined antennas")

def plot_radiation_pattern(theta, phi, PCG_gain):
    PCG_gain_scal = np.power(10, PCG_gain/10)
    x=[]
    y=[]
    z=[]
    for i in range(len(theta)):
        t = np.deg2rad(theta[i])
        p = np.deg2rad(phi[i])
        x.append(PCG_gain_scal[i]*np.sin(t)*np.cos(p))
        y.append(PCG_gain_scal[i]*np.sin(t)*np.sin(p))
        z.append(PCG_gain_scal[i]*np.cos(t))

    fig= plt.figure()
    ax = Axes3D(fig)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.view_init(90, 0)
    scat = ax.scatter3D(np.array(x), np.array(y), np.array(z), c = PCG_gain_scal)
    fig.colorbar(scat)
    ax.set_title("Radiation pattern satellite combined antennas")

def plot_radiation_pattern_2D(theta, PCG_gain):
    fig= plt.figure()
    ax = fig.add_subplot(111)
    angle_lim = 361
    for i in range(36):
        ax.plot(theta[i*angle_lim : (i+1)*angle_lim], PCG_gain[i*angle_lim : (i+1)*angle_lim])
    
    ax.set_xlabel('Theta[deg]')
    ax.set_ylabel('Antennas gain[dBi]')
    ax.set_title("Radiation pattern satellite combined antennas")

def plot_phi_constant(phi_single):
    fig2, ax2 = plt.subplots() 
    theta_phi_constant = []
    gain_phi_constant = []
    for t in range(len(theta)):
        if phi[t] == phi_single: 
            theta_phi_constant.append(theta[t])
            gain_phi_constant.append(PCG_gain[t])
    ax2.plot(theta_phi_constant, gain_phi_constant)
    ax2.set_title("Combined Anywaves Band S Antenna gain : 2218MHz, phi = " +  str(phi_single) + " deg")
    ax2.set_xlabel("Theta (deg)")
    ax2.set_ylabel("Combined gain (dBi)")

def plot_theta_constant(theta_single):
    fig3, ax3 = plt.subplots()
    phi_theta_constant = []
    gain_theta_constant = []
    for t in range(len(theta)):
        if theta[t] == theta_single: 
            phi_theta_constant.append(phi[t])
            gain_theta_constant.append(PCG_gain[t])
    ax3.plot(phi_theta_constant, gain_theta_constant)
    ax3.set_title("Combined Anywaves Band S Antenna gain : 2218MHz, theta = " +  str(theta_angle) + " deg")
    ax3.set_xlabel("Phi (deg)")
    ax3.set_ylabel("Combined gain (dBi)")


if __name__ == "__main__": 
    percentile = 50
    filepath = 'BandeS_PCD-PCG-2218M-norme.txt'
    phi_angle = 45
    theta_angle = 45

    [theta, phi, PCG_gain] = read_antenna_data_file(filepath)
    plot_radiation_pattern(theta, phi, PCG_gain)
    plot_phi_constant(phi_angle)
    plot_theta_constant(theta_angle)
    plot_radiation_pattern_2D(theta, PCG_gain)
    plot_depointing_gain(theta, phi, PCG_gain) 
    plt.show()
    print('antenna_data_file = ' + filepath)
    print(100-percentile, "% of gain data are above : ", np.percentile(PCG_gain, percentile), "dBi") 


