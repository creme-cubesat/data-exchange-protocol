# Data Exchange Protocol of CREME CubeSat

This GitLab repository contains:
* An investigation report (Report folder)
* Presentation (Presentation folder)
* Python files for simulations (Python folder)
* All relevant research papers (State of the Art)

The Simulation of Tumbling and the Erasure Correcting Code are in the following repository:
Python/SatelliteTumbling

Within this folder, there are 3 python files:
* A clip maker of satellite's orbit and attitude in 3D space (ClipMakerOfTumbling.py)
* Simulation of satellite's astrodynamics and antenna model (SimulationOfTumbling.py)
* Implementation of a BEC and zfec (ErasureCorrectingCode.py)

The input to all of the previous files is a yaml file, and it is found in the following repository:
Python/SatelliteTumbling/data/input.yaml
